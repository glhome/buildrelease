#!/bin/bash

# parsing xml using xmlstarlib
# xml sel -t -v 'tagtree/@' -nl default.xml 

XML_FILE=default.xml

# extract xml tree first
XML_TREE=$(xml el default.xml | tr ' ' '\n' | sort -u | tr '\n' ' ')

echo $XML_TREE

# function to clone git repo and checkoy the revisions
function clone_repo(){
    repo=$1
    mkdir $(pwd)/backup | cd backup
    # test repo
    repo="https://github.com/glhome/buildrelease.git"
    git clone $repo $(pwd)/backup
}

function get_commits(){
    commit=$1
    cd $(pwd)/backup
    git checkout $commit
}


# get elements from XML_TREE

for path in $XML_TREE[@]; do
    if [ $path == 'manifest/remote' ]; then
        REPO_URL=$(xml sel -t -v "//$path/@fetch" -nl default.xml)
        for repo in $REPO_URL[@]; do
            clone_repo $repo
        done
        REPO_NAME=$(xml sel -t -v "//$path/@name" -nl default.xml)
    elif [ $path == 'manifest/project' ]; then
        REPO_REVISION=$(xml sel -t -v "//$path/@revision" -nl default.xml)
    fi
done

echo $REPO_URL
echo $REPO_NAME
echo $REPO_REVISION

