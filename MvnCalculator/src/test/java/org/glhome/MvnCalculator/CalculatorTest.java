package org.glhome.MvnCalculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;

import org.junit.Test;

public class CalculatorTest {
	
	 @Test
	  public void evaluatesExpressionTrue() {
	    Calculator calculator = new Calculator();
	    int sum = calculator.evaluate("1+2+3");
	    assertEquals(6, sum);

	 }
	 @Test
	 public void evaluatesExpressionFalse() {
	    Calculator calculator = new Calculator();
	    int sum = calculator.evaluate("3+2+3");
	    assertEquals(8, sum);
	 }
	 @Test
	 public void evaluatesNotEqual() {
	    Calculator calculator = new Calculator();
	    int sum = calculator.evaluate("3+2+3");
	    assertNotSame(9, sum);
	 }

}
