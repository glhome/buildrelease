package gradleCalculator;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorTest {

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	@Test
	  public void evaluatesExpressionTrue() {
	    Calculator calculator = new Calculator();
	    int sum = calculator.evaluate("1+2+3");
	    assertEquals(6, sum);

	 }
	 @Test
	 public void evaluatesExpressionFalse() {
	    Calculator calculator = new Calculator();
	    int sum = calculator.evaluate("3+2+3");
	    assertEquals(8, sum);
	 }
	 @Test
	 public void evaluatesNotEqual() {
	    Calculator calculator = new Calculator();
	    int sum = calculator.evaluate("3+2+3");
	    assertNotSame(9, sum);
	 }

}
